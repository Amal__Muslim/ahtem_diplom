package com.diplom.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.diplom.app.service.ShopItemService;
import com.diplom.app.web.rest.errors.BadRequestAlertException;
import com.diplom.app.web.rest.util.HeaderUtil;
import com.diplom.app.service.dto.ShopItemDTO;
import com.diplom.app.service.dto.ShopItemCriteria;
import com.diplom.app.service.ShopItemQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ShopItem.
 */
@RestController
@RequestMapping("/api")
public class ShopItemResource {

    private final Logger log = LoggerFactory.getLogger(ShopItemResource.class);

    private static final String ENTITY_NAME = "shopItem";

    private final ShopItemService shopItemService;

    private final ShopItemQueryService shopItemQueryService;

    public ShopItemResource(ShopItemService shopItemService, ShopItemQueryService shopItemQueryService) {
        this.shopItemService = shopItemService;
        this.shopItemQueryService = shopItemQueryService;
    }

    /**
     * POST  /shop-items : Create a new shopItem.
     *
     * @param shopItemDTO the shopItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shopItemDTO, or with status 400 (Bad Request) if the shopItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shop-items")
    @Timed
    public ResponseEntity<ShopItemDTO> createShopItem(@RequestBody ShopItemDTO shopItemDTO) throws URISyntaxException {
        log.debug("REST request to save ShopItem : {}", shopItemDTO);
        if (shopItemDTO.getId() != null) {
            throw new BadRequestAlertException("A new shopItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShopItemDTO result = shopItemService.save(shopItemDTO);
        return ResponseEntity.created(new URI("/api/shop-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /shop-items : Updates an existing shopItem.
     *
     * @param shopItemDTO the shopItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shopItemDTO,
     * or with status 400 (Bad Request) if the shopItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the shopItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shop-items")
    @Timed
    public ResponseEntity<ShopItemDTO> updateShopItem(@RequestBody ShopItemDTO shopItemDTO) throws URISyntaxException {
        log.debug("REST request to update ShopItem : {}", shopItemDTO);
        if (shopItemDTO.getId() == null) {
            return createShopItem(shopItemDTO);
        }
        ShopItemDTO result = shopItemService.save(shopItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shopItemDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /shop-items : get all the shopItems.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of shopItems in body
     */
    @GetMapping("/shop-items")
    @Timed
    public ResponseEntity<List<ShopItemDTO>> getAllShopItems(ShopItemCriteria criteria) {
        log.debug("REST request to get ShopItems by criteria: {}", criteria);
        List<ShopItemDTO> entityList = shopItemQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /shop-items/:id : get the "id" shopItem.
     *
     * @param id the id of the shopItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shopItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shop-items/{id}")
    @Timed
    public ResponseEntity<ShopItemDTO> getShopItem(@PathVariable Long id) {
        log.debug("REST request to get ShopItem : {}", id);
        ShopItemDTO shopItemDTO = shopItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shopItemDTO));
    }

    /**
     * DELETE  /shop-items/:id : delete the "id" shopItem.
     *
     * @param id the id of the shopItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shop-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteShopItem(@PathVariable Long id) {
        log.debug("REST request to delete ShopItem : {}", id);
        shopItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
