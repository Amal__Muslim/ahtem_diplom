/**
 * View Models used by Spring MVC REST controllers.
 */
package com.diplom.app.web.rest.vm;
