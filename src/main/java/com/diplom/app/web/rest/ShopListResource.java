package com.diplom.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.diplom.app.service.ShopListService;
import com.diplom.app.web.rest.errors.BadRequestAlertException;
import com.diplom.app.web.rest.util.HeaderUtil;
import com.diplom.app.service.dto.ShopListDTO;
import com.diplom.app.service.dto.ShopListCriteria;
import com.diplom.app.service.ShopListQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ShopList.
 */
@RestController
@RequestMapping("/api")
public class ShopListResource {

    private final Logger log = LoggerFactory.getLogger(ShopListResource.class);

    private static final String ENTITY_NAME = "shopList";

    private final ShopListService shopListService;

    private final ShopListQueryService shopListQueryService;

    public ShopListResource(ShopListService shopListService, ShopListQueryService shopListQueryService) {
        this.shopListService = shopListService;
        this.shopListQueryService = shopListQueryService;
    }

    /**
     * POST  /shop-lists : Create a new shopList.
     *
     * @param shopListDTO the shopListDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shopListDTO, or with status 400 (Bad Request) if the shopList has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shop-lists")
    @Timed
    public ResponseEntity<ShopListDTO> createShopList(@RequestBody ShopListDTO shopListDTO) throws URISyntaxException {
        log.debug("REST request to save ShopList : {}", shopListDTO);
        if (shopListDTO.getId() != null) {
            throw new BadRequestAlertException("A new shopList cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShopListDTO result = shopListService.save(shopListDTO);
        return ResponseEntity.created(new URI("/api/shop-lists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /shop-lists : Updates an existing shopList.
     *
     * @param shopListDTO the shopListDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shopListDTO,
     * or with status 400 (Bad Request) if the shopListDTO is not valid,
     * or with status 500 (Internal Server Error) if the shopListDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shop-lists")
    @Timed
    public ResponseEntity<ShopListDTO> updateShopList(@RequestBody ShopListDTO shopListDTO) throws URISyntaxException {
        log.debug("REST request to update ShopList : {}", shopListDTO);
        if (shopListDTO.getId() == null) {
            return createShopList(shopListDTO);
        }
        ShopListDTO result = shopListService.save(shopListDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shopListDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /shop-lists : get all the shopLists.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of shopLists in body
     */
    @GetMapping("/shop-lists")
    @Timed
    public ResponseEntity<List<ShopListDTO>> getAllShopLists(ShopListCriteria criteria) {
        log.debug("REST request to get ShopLists by criteria: {}", criteria);
        List<ShopListDTO> entityList = shopListQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /shop-lists/:id : get the "id" shopList.
     *
     * @param id the id of the shopListDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shopListDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shop-lists/{id}")
    @Timed
    public ResponseEntity<ShopListDTO> getShopList(@PathVariable Long id) {
        log.debug("REST request to get ShopList : {}", id);
        ShopListDTO shopListDTO = shopListService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shopListDTO));
    }

    /**
     * DELETE  /shop-lists/:id : delete the "id" shopList.
     *
     * @param id the id of the shopListDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shop-lists/{id}")
    @Timed
    public ResponseEntity<Void> deleteShopList(@PathVariable Long id) {
        log.debug("REST request to delete ShopList : {}", id);
        shopListService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
