package com.diplom.app.service.dto;


import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the ShopItem entity.
 */
public class ShopItemDTO implements Serializable {

    private Long id;

    private Integer count;

    private ZonedDateTime createDate;

    private Long shopListId;

    private String shopListFullName;

    private String shopListPhone;

    private Long productId;

    private String productName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getShopListId() {
        return shopListId;
    }

    public void setShopListId(Long shopListId) {
        this.shopListId = shopListId;
    }

    public String getShopListFullName() {
        return shopListFullName;
    }

    public void setShopListFullName(String shopListFullName) {
        this.shopListFullName = shopListFullName;
    }

    public String getShopListPhone() {
        return shopListPhone;
    }

    public void setShopListPhone(String shopListPhone) {
        this.shopListPhone = shopListPhone;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShopItemDTO shopItemDTO = (ShopItemDTO) o;
        if(shopItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shopItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShopItemDTO{" +
            "id=" + getId() +
            ", count=" + getCount() +
            ", createDate='" + getCreateDate() + "'" +
            "}";
    }
}
