package com.diplom.app.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Product entity. This class is used in ProductResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /products?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProductCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private IntegerFilter price;

    private IntegerFilter previousPrice;

    private IntegerFilter count;

    private IntegerFilter stars;

    private StringFilter details;

    private ZonedDateTimeFilter createDate;

    private LongFilter shopItemsId;

    public ProductCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public IntegerFilter getPrice() {
        return price;
    }

    public void setPrice(IntegerFilter price) {
        this.price = price;
    }

    public IntegerFilter getPreviousPrice() {
        return previousPrice;
    }

    public void setPreviousPrice(IntegerFilter previousPrice) {
        this.previousPrice = previousPrice;
    }

    public IntegerFilter getCount() {
        return count;
    }

    public void setCount(IntegerFilter count) {
        this.count = count;
    }

    public IntegerFilter getStars() {
        return stars;
    }

    public void setStars(IntegerFilter stars) {
        this.stars = stars;
    }

    public StringFilter getDetails() {
        return details;
    }

    public void setDetails(StringFilter details) {
        this.details = details;
    }

    public ZonedDateTimeFilter getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTimeFilter createDate) {
        this.createDate = createDate;
    }

    public LongFilter getShopItemsId() {
        return shopItemsId;
    }

    public void setShopItemsId(LongFilter shopItemsId) {
        this.shopItemsId = shopItemsId;
    }

    @Override
    public String toString() {
        return "ProductCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (price != null ? "price=" + price + ", " : "") +
                (previousPrice != null ? "previousPrice=" + previousPrice + ", " : "") +
                (count != null ? "count=" + count + ", " : "") +
                (stars != null ? "stars=" + stars + ", " : "") +
                (details != null ? "details=" + details + ", " : "") +
                (createDate != null ? "createDate=" + createDate + ", " : "") +
                (shopItemsId != null ? "shopItemsId=" + shopItemsId + ", " : "") +
            "}";
    }

}
