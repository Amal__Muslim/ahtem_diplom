package com.diplom.app.service.mapper;

import com.diplom.app.domain.*;
import com.diplom.app.service.dto.ShopListDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ShopList and its DTO ShopListDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ShopListMapper extends EntityMapper<ShopListDTO, ShopList> {


    @Mapping(target = "shopItems", ignore = true)
    ShopList toEntity(ShopListDTO shopListDTO);

    default ShopList fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShopList shopList = new ShopList();
        shopList.setId(id);
        return shopList;
    }
}
