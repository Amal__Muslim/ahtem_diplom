package com.diplom.app.service.mapper;

import com.diplom.app.domain.*;
import com.diplom.app.service.dto.ShopItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ShopItem and its DTO ShopItemDTO.
 */
@Mapper(componentModel = "spring", uses = {ShopListMapper.class, ProductMapper.class})
public interface ShopItemMapper extends EntityMapper<ShopItemDTO, ShopItem> {

    @Mapping(source = "shopList.id", target = "shopListId")
    @Mapping(source = "shopList.fullName", target = "shopListFullName")
    @Mapping(source = "shopList.phone", target = "shopListPhone")
    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "product.name", target = "productName")
    ShopItemDTO toDto(ShopItem shopItem);

    @Mapping(source = "shopListId", target = "shopList")
    @Mapping(source = "productId", target = "product")
    ShopItem toEntity(ShopItemDTO shopItemDTO);

    default ShopItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShopItem shopItem = new ShopItem();
        shopItem.setId(id);
        return shopItem;
    }
}
