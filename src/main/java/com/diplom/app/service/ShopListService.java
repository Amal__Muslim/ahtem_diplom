package com.diplom.app.service;

import com.diplom.app.domain.ShopList;
import com.diplom.app.repository.ShopListRepository;
import com.diplom.app.service.dto.ShopListDTO;
import com.diplom.app.service.mapper.ShopListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ShopList.
 */
@Service
@Transactional
public class ShopListService {

    private final Logger log = LoggerFactory.getLogger(ShopListService.class);

    private final ShopListRepository shopListRepository;

    private final ShopListMapper shopListMapper;

    public ShopListService(ShopListRepository shopListRepository, ShopListMapper shopListMapper) {
        this.shopListRepository = shopListRepository;
        this.shopListMapper = shopListMapper;
    }

    /**
     * Save a shopList.
     *
     * @param shopListDTO the entity to save
     * @return the persisted entity
     */
    public ShopListDTO save(ShopListDTO shopListDTO) {
        log.debug("Request to save ShopList : {}", shopListDTO);
        ShopList shopList = shopListMapper.toEntity(shopListDTO);
        shopList = shopListRepository.save(shopList);
        return shopListMapper.toDto(shopList);
    }

    /**
     * Get all the shopLists.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ShopListDTO> findAll() {
        log.debug("Request to get all ShopLists");
        return shopListRepository.findAll().stream()
            .map(shopListMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one shopList by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ShopListDTO findOne(Long id) {
        log.debug("Request to get ShopList : {}", id);
        ShopList shopList = shopListRepository.findOne(id);
        return shopListMapper.toDto(shopList);
    }

    /**
     * Delete the shopList by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ShopList : {}", id);
        shopListRepository.delete(id);
    }
}
