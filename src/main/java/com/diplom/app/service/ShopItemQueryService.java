package com.diplom.app.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.diplom.app.domain.ShopItem;
import com.diplom.app.domain.*; // for static metamodels
import com.diplom.app.repository.ShopItemRepository;
import com.diplom.app.service.dto.ShopItemCriteria;

import com.diplom.app.service.dto.ShopItemDTO;
import com.diplom.app.service.mapper.ShopItemMapper;

/**
 * Service for executing complex queries for ShopItem entities in the database.
 * The main input is a {@link ShopItemCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShopItemDTO} or a {@link Page} of {@link ShopItemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShopItemQueryService extends QueryService<ShopItem> {

    private final Logger log = LoggerFactory.getLogger(ShopItemQueryService.class);


    private final ShopItemRepository shopItemRepository;

    private final ShopItemMapper shopItemMapper;

    public ShopItemQueryService(ShopItemRepository shopItemRepository, ShopItemMapper shopItemMapper) {
        this.shopItemRepository = shopItemRepository;
        this.shopItemMapper = shopItemMapper;
    }

    /**
     * Return a {@link List} of {@link ShopItemDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShopItemDTO> findByCriteria(ShopItemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ShopItem> specification = createSpecification(criteria);
        return shopItemMapper.toDto(shopItemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShopItemDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShopItemDTO> findByCriteria(ShopItemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ShopItem> specification = createSpecification(criteria);
        final Page<ShopItem> result = shopItemRepository.findAll(specification, page);
        return result.map(shopItemMapper::toDto);
    }

    /**
     * Function to convert ShopItemCriteria to a {@link Specifications}
     */
    private Specifications<ShopItem> createSpecification(ShopItemCriteria criteria) {
        Specifications<ShopItem> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ShopItem_.id));
            }
            if (criteria.getCount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCount(), ShopItem_.count));
            }
            if (criteria.getCreateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreateDate(), ShopItem_.createDate));
            }
            if (criteria.getShopListId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getShopListId(), ShopItem_.shopList, ShopList_.id));
            }
            if (criteria.getProductId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getProductId(), ShopItem_.product, Product_.id));
            }
        }
        return specification;
    }

}
