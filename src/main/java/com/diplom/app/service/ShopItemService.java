package com.diplom.app.service;

import com.diplom.app.domain.ShopItem;
import com.diplom.app.repository.ShopItemRepository;
import com.diplom.app.service.dto.ShopItemDTO;
import com.diplom.app.service.mapper.ShopItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ShopItem.
 */
@Service
@Transactional
public class ShopItemService {

    private final Logger log = LoggerFactory.getLogger(ShopItemService.class);

    private final ShopItemRepository shopItemRepository;

    private final ShopItemMapper shopItemMapper;

    public ShopItemService(ShopItemRepository shopItemRepository, ShopItemMapper shopItemMapper) {
        this.shopItemRepository = shopItemRepository;
        this.shopItemMapper = shopItemMapper;
    }

    /**
     * Save a shopItem.
     *
     * @param shopItemDTO the entity to save
     * @return the persisted entity
     */
    public ShopItemDTO save(ShopItemDTO shopItemDTO) {
        log.debug("Request to save ShopItem : {}", shopItemDTO);
        ShopItem shopItem = shopItemMapper.toEntity(shopItemDTO);
        shopItem = shopItemRepository.save(shopItem);
        return shopItemMapper.toDto(shopItem);
    }

    /**
     * Get all the shopItems.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ShopItemDTO> findAll() {
        log.debug("Request to get all ShopItems");
        return shopItemRepository.findAll().stream()
            .map(shopItemMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one shopItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ShopItemDTO findOne(Long id) {
        log.debug("Request to get ShopItem : {}", id);
        ShopItem shopItem = shopItemRepository.findOne(id);
        return shopItemMapper.toDto(shopItem);
    }

    /**
     * Delete the shopItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ShopItem : {}", id);
        shopItemRepository.delete(id);
    }
}
