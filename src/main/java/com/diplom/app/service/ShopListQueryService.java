package com.diplom.app.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.diplom.app.domain.ShopList;
import com.diplom.app.domain.*; // for static metamodels
import com.diplom.app.repository.ShopListRepository;
import com.diplom.app.service.dto.ShopListCriteria;

import com.diplom.app.service.dto.ShopListDTO;
import com.diplom.app.service.mapper.ShopListMapper;

/**
 * Service for executing complex queries for ShopList entities in the database.
 * The main input is a {@link ShopListCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShopListDTO} or a {@link Page} of {@link ShopListDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShopListQueryService extends QueryService<ShopList> {

    private final Logger log = LoggerFactory.getLogger(ShopListQueryService.class);


    private final ShopListRepository shopListRepository;

    private final ShopListMapper shopListMapper;

    public ShopListQueryService(ShopListRepository shopListRepository, ShopListMapper shopListMapper) {
        this.shopListRepository = shopListRepository;
        this.shopListMapper = shopListMapper;
    }

    /**
     * Return a {@link List} of {@link ShopListDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShopListDTO> findByCriteria(ShopListCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ShopList> specification = createSpecification(criteria);
        return shopListMapper.toDto(shopListRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShopListDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShopListDTO> findByCriteria(ShopListCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ShopList> specification = createSpecification(criteria);
        final Page<ShopList> result = shopListRepository.findAll(specification, page);
        return result.map(shopListMapper::toDto);
    }

    /**
     * Function to convert ShopListCriteria to a {@link Specifications}
     */
    private Specifications<ShopList> createSpecification(ShopListCriteria criteria) {
        Specifications<ShopList> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ShopList_.id));
            }
            if (criteria.getFullName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFullName(), ShopList_.fullName));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), ShopList_.address));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), ShopList_.phone));
            }
            if (criteria.getCreateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreateDate(), ShopList_.createDate));
            }
            if (criteria.getShopItemsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getShopItemsId(), ShopList_.shopItems, ShopItem_.id));
            }
        }
        return specification;
    }

}
