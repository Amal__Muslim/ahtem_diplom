package com.diplom.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Integer price;

    @Column(name = "previous_price")
    private Integer previousPrice;

    @Column(name = "count")
    private Integer count;

    @Column(name = "stars")
    private Integer stars;

    @Column(name = "details")
    private String details;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private Set<ShopItem> shopItems = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Product name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public Product price(Integer price) {
        this.price = price;
        return this;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPreviousPrice() {
        return previousPrice;
    }

    public Product previousPrice(Integer previousPrice) {
        this.previousPrice = previousPrice;
        return this;
    }

    public void setPreviousPrice(Integer previousPrice) {
        this.previousPrice = previousPrice;
    }

    public Integer getCount() {
        return count;
    }

    public Product count(Integer count) {
        this.count = count;
        return this;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getStars() {
        return stars;
    }

    public Product stars(Integer stars) {
        this.stars = stars;
        return this;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getDetails() {
        return details;
    }

    public Product details(String details) {
        this.details = details;
        return this;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public byte[] getImage() {
        return image;
    }

    public Product image(byte[] image) {
        this.image = image;
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public Product imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public Product createDate(ZonedDateTime createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public Set<ShopItem> getShopItems() {
        return shopItems;
    }

    public Product shopItems(Set<ShopItem> shopItems) {
        this.shopItems = shopItems;
        return this;
    }

    public Product addShopItems(ShopItem shopItem) {
        this.shopItems.add(shopItem);
        shopItem.setProduct(this);
        return this;
    }

    public Product removeShopItems(ShopItem shopItem) {
        this.shopItems.remove(shopItem);
        shopItem.setProduct(null);
        return this;
    }

    public void setShopItems(Set<ShopItem> shopItems) {
        this.shopItems = shopItems;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Product product = (Product) o;
        if (product.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), product.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", price=" + getPrice() +
            ", previousPrice=" + getPreviousPrice() +
            ", count=" + getCount() +
            ", stars=" + getStars() +
            ", details='" + getDetails() + "'" +
            ", image='" + getImage() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            "}";
    }
}
