package com.diplom.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ShopList.
 */
@Entity
@Table(name = "shop_list")
public class ShopList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @OneToMany(mappedBy = "shopList")
    @JsonIgnore
    private Set<ShopItem> shopItems = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public ShopList fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public ShopList address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public ShopList phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public ShopList createDate(ZonedDateTime createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public Set<ShopItem> getShopItems() {
        return shopItems;
    }

    public ShopList shopItems(Set<ShopItem> shopItems) {
        this.shopItems = shopItems;
        return this;
    }

    public ShopList addShopItems(ShopItem shopItem) {
        this.shopItems.add(shopItem);
        shopItem.setShopList(this);
        return this;
    }

    public ShopList removeShopItems(ShopItem shopItem) {
        this.shopItems.remove(shopItem);
        shopItem.setShopList(null);
        return this;
    }

    public void setShopItems(Set<ShopItem> shopItems) {
        this.shopItems = shopItems;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShopList shopList = (ShopList) o;
        if (shopList.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shopList.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShopList{" +
            "id=" + getId() +
            ", fullName='" + getFullName() + "'" +
            ", address='" + getAddress() + "'" +
            ", phone='" + getPhone() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            "}";
    }
}
