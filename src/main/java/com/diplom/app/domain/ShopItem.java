package com.diplom.app.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A ShopItem.
 */
@Entity
@Table(name = "shop_item")
public class ShopItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "count")
    private Integer count;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @ManyToOne
    private ShopList shopList;

    @ManyToOne
    private Product product;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCount() {
        return count;
    }

    public ShopItem count(Integer count) {
        this.count = count;
        return this;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public ShopItem createDate(ZonedDateTime createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ShopList getShopList() {
        return shopList;
    }

    public ShopItem shopList(ShopList shopList) {
        this.shopList = shopList;
        return this;
    }

    public void setShopList(ShopList shopList) {
        this.shopList = shopList;
    }

    public Product getProduct() {
        return product;
    }

    public ShopItem product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShopItem shopItem = (ShopItem) o;
        if (shopItem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shopItem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShopItem{" +
            "id=" + getId() +
            ", count=" + getCount() +
            ", createDate='" + getCreateDate() + "'" +
            "}";
    }
}
