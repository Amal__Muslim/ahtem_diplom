package com.diplom.app.repository;

import com.diplom.app.domain.ShopItem;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ShopItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShopItemRepository extends JpaRepository<ShopItem, Long>, JpaSpecificationExecutor<ShopItem> {

}
