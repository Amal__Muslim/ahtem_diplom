package com.diplom.app.repository;

import com.diplom.app.domain.ShopList;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ShopList entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShopListRepository extends JpaRepository<ShopList, Long>, JpaSpecificationExecutor<ShopList> {

}
