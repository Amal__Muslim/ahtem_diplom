(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('stat', {
            parent: 'app',
            url: '/stat',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'app/stat/stat.html',
                    controller: 'StatController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('product');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
