(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('StatController', StatController);

    StatController.$inject = ['Principal', 'LoginService', '$state', '$scope', 'ShopItem', 'Product', 'ShopList'];

    function StatController (Principal, LoginService, $state, $scope, ShopItem, Product, ShopList) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.productName = null;
        vm.ID = null;
        vm.run = run;

        vm.SOLD = 0;
        vm.REVENUE = 0;
        vm.ORDERS = 0;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        function register () {
            $state.go('register');
        }

        function run() {
            var today = new Date();
            var priorDate = new Date().setDate(today.getDate() - 30);
            ShopItem.query({ 'productId.equals': vm.ID, 'createDate.greaterThan': new Date(priorDate) }, function (shopItems) {
                Product.get({ id: vm.ID }, function (product) {
                    vm.productName = product.name;
                    vm.SOLD = shopItems.reduce(function (prev, curr) {
                        return prev + curr.count;
                    }, 0);
                    vm.REVENUE = vm.SOLD * product.price;
                    vm.ORDERS = shopItems.length;
                });
            });
        }
    }
})();
