(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state', 'Product', 'AlertService', '$rootScope'];

    function HomeController ($scope, Principal, LoginService, $state, Product, AlertService, $rootScope) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.addToCart = addToCart;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();
        loadAll();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        function register () {
            $state.go('register');
        }

        function addToCart(product) {
            var hasProduct = $rootScope.cart.some(function (elem) {
                return elem.id === product.id;
            });
            if (hasProduct) return ;
            $rootScope.cart.push(product);
            AlertService.success('Добавлено в корзину');
        }

        function loadAll () {
            Product.query({
                page: 0,
                size: 30
            }, onSuccess, onError);
            function onSuccess(data, headers) {
                vm.products = data;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }
    }
})();
