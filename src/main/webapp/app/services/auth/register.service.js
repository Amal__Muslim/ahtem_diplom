(function () {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
