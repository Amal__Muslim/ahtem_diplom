(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('ShopController', ShopController);

    ShopController.$inject = ['$scope', 'Principal', 'LoginService',
        '$state', 'Product', 'AlertService', 'DataUtils', 'ParseLinks', 'paginationConstants', 'pagingParams', '$rootScope'];

    function ShopController($scope, Principal, LoginService, $state, Product, AlertService, 
        DataUtils, ParseLinks, paginationConstants, pagingParams, $rootScope) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.isList = false;

        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.descending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
        vm.changeView = changeView;
        vm.addToCart = addToCart;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();
        loadTop();
        loadAll();

        function changeView(value) {
            vm.isList = value;
        }

        function addToCart(product) {
            var hasProduct = $rootScope.cart.some(function (elem) {
                return elem.id === product.id;
            });
            if (hasProduct) return ;
            $rootScope.cart.push(product);
            AlertService.error( 'Добавлено в корзину');
        }

        function loadAll () {
            Product.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.products = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        function register () {
            $state.go('register');
        }


        function loadTop () {
            Product.query({
                page: 0,
                size: 3
            }, onSuccess, onError);
            function onSuccess(data, headers) {
                vm.topProducts = data;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }
    }
})();
