(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('shop-list', {
            parent: 'entity',
            url: '/shop-list',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ahtemDiplomApp.shopList.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/shop-list/shop-lists.html',
                    controller: 'ShopListController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('shopList');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('shop-list-detail', {
            parent: 'shop-list',
            url: '/shop-list/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ahtemDiplomApp.shopList.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/shop-list/shop-list-detail.html',
                    controller: 'ShopListDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('shopList');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ShopList', function($stateParams, ShopList) {
                    return ShopList.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'shop-list',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('shop-list-detail.edit', {
            parent: 'shop-list-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shop-list/shop-list-dialog.html',
                    controller: 'ShopListDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ShopList', function(ShopList) {
                            return ShopList.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('shop-list.new', {
            parent: 'shop-list',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shop-list/shop-list-dialog.html',
                    controller: 'ShopListDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                fullName: null,
                                address: null,
                                phone: null,
                                createDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('shop-list', null, { reload: 'shop-list' });
                }, function() {
                    $state.go('shop-list');
                });
            }]
        })
        .state('shop-list.edit', {
            parent: 'shop-list',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shop-list/shop-list-dialog.html',
                    controller: 'ShopListDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ShopList', function(ShopList) {
                            return ShopList.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('shop-list', null, { reload: 'shop-list' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('shop-list.delete', {
            parent: 'shop-list',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shop-list/shop-list-delete-dialog.html',
                    controller: 'ShopListDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ShopList', function(ShopList) {
                            return ShopList.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('shop-list', null, { reload: 'shop-list' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
