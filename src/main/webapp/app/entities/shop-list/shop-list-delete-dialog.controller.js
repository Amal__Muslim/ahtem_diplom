(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('ShopListDeleteController',ShopListDeleteController);

    ShopListDeleteController.$inject = ['$uibModalInstance', 'entity', 'ShopList'];

    function ShopListDeleteController($uibModalInstance, entity, ShopList) {
        var vm = this;

        vm.shopList = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ShopList.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
