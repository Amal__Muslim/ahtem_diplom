(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('ShopListController', ShopListController);

    ShopListController.$inject = ['ShopList'];

    function ShopListController(ShopList) {

        var vm = this;

        vm.shopLists = [];

        loadAll();

        function loadAll() {
            ShopList.query(function(result) {
                vm.shopLists = result;
                vm.searchQuery = null;
            });
        }
    }
})();
