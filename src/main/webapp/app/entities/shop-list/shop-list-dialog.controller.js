(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('ShopListDialogController', ShopListDialogController);

    ShopListDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ShopList', 'ShopItem'];

    function ShopListDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ShopList, ShopItem) {
        var vm = this;

        vm.shopList = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.shopitems = ShopItem.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.shopList.id !== null) {
                ShopList.update(vm.shopList, onSaveSuccess, onSaveError);
            } else {
                ShopList.save(vm.shopList, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('ahtemDiplomApp:shopListUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
