(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('ShopListDetailController', ShopListDetailController);

    ShopListDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ShopList', 'ShopItem'];

    function ShopListDetailController($scope, $rootScope, $stateParams, previousState, entity, ShopList, ShopItem) {
        var vm = this;

        vm.shopList = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('ahtemDiplomApp:shopListUpdate', function(event, result) {
            vm.shopList = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
