(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('shop-item', {
            parent: 'entity',
            url: '/shop-item',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ahtemDiplomApp.shopItem.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/shop-item/shop-items.html',
                    controller: 'ShopItemController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('shopItem');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('shop-item-detail', {
            parent: 'shop-item',
            url: '/shop-item/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ahtemDiplomApp.shopItem.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/shop-item/shop-item-detail.html',
                    controller: 'ShopItemDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('shopItem');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ShopItem', function($stateParams, ShopItem) {
                    return ShopItem.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'shop-item',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('shop-item-detail.edit', {
            parent: 'shop-item-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shop-item/shop-item-dialog.html',
                    controller: 'ShopItemDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ShopItem', function(ShopItem) {
                            return ShopItem.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('shop-item.new', {
            parent: 'shop-item',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shop-item/shop-item-dialog.html',
                    controller: 'ShopItemDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                count: null,
                                createDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('shop-item', null, { reload: 'shop-item' });
                }, function() {
                    $state.go('shop-item');
                });
            }]
        })
        .state('shop-item.edit', {
            parent: 'shop-item',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shop-item/shop-item-dialog.html',
                    controller: 'ShopItemDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ShopItem', function(ShopItem) {
                            return ShopItem.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('shop-item', null, { reload: 'shop-item' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('shop-item.delete', {
            parent: 'shop-item',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shop-item/shop-item-delete-dialog.html',
                    controller: 'ShopItemDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ShopItem', function(ShopItem) {
                            return ShopItem.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('shop-item', null, { reload: 'shop-item' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
