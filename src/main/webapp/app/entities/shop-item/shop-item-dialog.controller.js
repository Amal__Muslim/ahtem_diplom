(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('ShopItemDialogController', ShopItemDialogController);

    ShopItemDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ShopItem', 'ShopList', 'Product'];

    function ShopItemDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ShopItem, ShopList, Product) {
        var vm = this;

        vm.shopItem = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.shoplists = ShopList.query();
        vm.products = Product.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.shopItem.id !== null) {
                ShopItem.update(vm.shopItem, onSaveSuccess, onSaveError);
            } else {
                ShopItem.save(vm.shopItem, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('ahtemDiplomApp:shopItemUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
