(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('ShopItemController', ShopItemController);

    ShopItemController.$inject = ['ShopItem'];

    function ShopItemController(ShopItem) {

        var vm = this;

        vm.shopItems = [];

        loadAll();

        function loadAll() {
            ShopItem.query(function(result) {
                vm.shopItems = result;
                vm.searchQuery = null;
            });
        }
    }
})();
