(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('cart', {
            parent: 'app',
            url: '/cart',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/cart/cart.html',
                    controller: 'CartController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('product');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
