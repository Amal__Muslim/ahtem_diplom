(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('CartController', CartController);

    CartController.$inject = ['$scope', 'Principal', 'LoginService', '$state', 'Product', 'AlertService', '$rootScope', 'ShopList', 'ShopItem'];

    function CartController ($scope, Principal, LoginService, $state, Product, AlertService, $rootScope, ShopList, ShopItem) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.products = $rootScope.cart;
        vm.send = send;
        vm.remove = remove;
        vm.shopList = {
            address: '',
            fullName: '',
            phone: '',
            createDate: new Date()
        };
        vm.shopItems = [];
        for (var i = 0; i < vm.products.length; i++) {
            var product = vm.products[i];
            vm.shopItems.push({
                count: 1,
                productId: product.id,
                product: product,
                shopListId: null,
                createDate: new Date()
            });
        }

        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        $scope.$watch('vm.shopItems', function() {
            vm.totalPrice = vm.shopItems.reduce(function (prev, cur) {
                return prev + cur.count * cur.product.price;
            }, 0);
        }, true);

        getAccount();


        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        function register () {
            $state.go('register');
        }

        function send() {
            if (vm.shopItems.length < 1) return;
            ShopList.save(vm.shopList, function (shopList) {
                for (var i = 0; i < vm.shopItems.length; i++) {
                    var shopItem = vm.shopItems[i];
                    shopItem.shopListId = shopList.id;
                    ShopItem.save(shopItem, function () {
                        $rootScope.cart = [];
                        vm.shopItems = [];
                        vm.products = [];
                    });
                }
            }, null);
        }

        function remove(shopItem) {
            vm.shopItems = vm.shopItems.filter(function (shI) {
                return shI.productId !== shopItem.productId;
            });
        }
    }
})();
