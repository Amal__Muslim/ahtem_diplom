(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .constant('paginationConstants', {
            'itemsPerPage': 15
        });
})();
