(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('ShopItemDetailController', ShopItemDetailController);

    ShopItemDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ShopItem', 'ShopList', 'Product'];

    function ShopItemDetailController($scope, $rootScope, $stateParams, previousState, entity, ShopItem, ShopList, Product) {
        var vm = this;

        vm.shopItem = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('ahtemDiplomApp:shopItemUpdate', function(event, result) {
            vm.shopItem = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
