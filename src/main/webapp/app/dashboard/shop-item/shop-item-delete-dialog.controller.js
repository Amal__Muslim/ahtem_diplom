(function() {
    'use strict';

    angular
        .module('ahtemDiplomApp')
        .controller('ShopItemDeleteController',ShopItemDeleteController);

    ShopItemDeleteController.$inject = ['$uibModalInstance', 'entity', 'ShopItem'];

    function ShopItemDeleteController($uibModalInstance, entity, ShopItem) {
        var vm = this;

        vm.shopItem = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ShopItem.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
