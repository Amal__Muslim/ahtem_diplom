package com.diplom.app.web.rest;

import com.diplom.app.AhtemDiplomApp;

import com.diplom.app.domain.ShopList;
import com.diplom.app.domain.ShopItem;
import com.diplom.app.repository.ShopListRepository;
import com.diplom.app.service.ShopListService;
import com.diplom.app.service.dto.ShopListDTO;
import com.diplom.app.service.mapper.ShopListMapper;
import com.diplom.app.web.rest.errors.ExceptionTranslator;
import com.diplom.app.service.dto.ShopListCriteria;
import com.diplom.app.service.ShopListQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.diplom.app.web.rest.TestUtil.sameInstant;
import static com.diplom.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ShopListResource REST controller.
 *
 * @see ShopListResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AhtemDiplomApp.class)
public class ShopListResourceIntTest {

    private static final String DEFAULT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FULL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private ShopListRepository shopListRepository;

    @Autowired
    private ShopListMapper shopListMapper;

    @Autowired
    private ShopListService shopListService;

    @Autowired
    private ShopListQueryService shopListQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restShopListMockMvc;

    private ShopList shopList;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShopListResource shopListResource = new ShopListResource(shopListService, shopListQueryService);
        this.restShopListMockMvc = MockMvcBuilders.standaloneSetup(shopListResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopList createEntity(EntityManager em) {
        ShopList shopList = new ShopList()
            .fullName(DEFAULT_FULL_NAME)
            .address(DEFAULT_ADDRESS)
            .phone(DEFAULT_PHONE)
            .createDate(DEFAULT_CREATE_DATE);
        return shopList;
    }

    @Before
    public void initTest() {
        shopList = createEntity(em);
    }

    @Test
    @Transactional
    public void createShopList() throws Exception {
        int databaseSizeBeforeCreate = shopListRepository.findAll().size();

        // Create the ShopList
        ShopListDTO shopListDTO = shopListMapper.toDto(shopList);
        restShopListMockMvc.perform(post("/api/shop-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopListDTO)))
            .andExpect(status().isCreated());

        // Validate the ShopList in the database
        List<ShopList> shopListList = shopListRepository.findAll();
        assertThat(shopListList).hasSize(databaseSizeBeforeCreate + 1);
        ShopList testShopList = shopListList.get(shopListList.size() - 1);
        assertThat(testShopList.getFullName()).isEqualTo(DEFAULT_FULL_NAME);
        assertThat(testShopList.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testShopList.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testShopList.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
    }

    @Test
    @Transactional
    public void createShopListWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shopListRepository.findAll().size();

        // Create the ShopList with an existing ID
        shopList.setId(1L);
        ShopListDTO shopListDTO = shopListMapper.toDto(shopList);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShopListMockMvc.perform(post("/api/shop-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopListDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShopList in the database
        List<ShopList> shopListList = shopListRepository.findAll();
        assertThat(shopListList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllShopLists() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList
        restShopListMockMvc.perform(get("/api/shop-lists?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopList.getId().intValue())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))));
    }

    @Test
    @Transactional
    public void getShopList() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get the shopList
        restShopListMockMvc.perform(get("/api/shop-lists/{id}", shopList.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shopList.getId().intValue()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)));
    }

    @Test
    @Transactional
    public void getAllShopListsByFullNameIsEqualToSomething() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where fullName equals to DEFAULT_FULL_NAME
        defaultShopListShouldBeFound("fullName.equals=" + DEFAULT_FULL_NAME);

        // Get all the shopListList where fullName equals to UPDATED_FULL_NAME
        defaultShopListShouldNotBeFound("fullName.equals=" + UPDATED_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllShopListsByFullNameIsInShouldWork() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where fullName in DEFAULT_FULL_NAME or UPDATED_FULL_NAME
        defaultShopListShouldBeFound("fullName.in=" + DEFAULT_FULL_NAME + "," + UPDATED_FULL_NAME);

        // Get all the shopListList where fullName equals to UPDATED_FULL_NAME
        defaultShopListShouldNotBeFound("fullName.in=" + UPDATED_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllShopListsByFullNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where fullName is not null
        defaultShopListShouldBeFound("fullName.specified=true");

        // Get all the shopListList where fullName is null
        defaultShopListShouldNotBeFound("fullName.specified=false");
    }

    @Test
    @Transactional
    public void getAllShopListsByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where address equals to DEFAULT_ADDRESS
        defaultShopListShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the shopListList where address equals to UPDATED_ADDRESS
        defaultShopListShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllShopListsByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultShopListShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the shopListList where address equals to UPDATED_ADDRESS
        defaultShopListShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllShopListsByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where address is not null
        defaultShopListShouldBeFound("address.specified=true");

        // Get all the shopListList where address is null
        defaultShopListShouldNotBeFound("address.specified=false");
    }

    @Test
    @Transactional
    public void getAllShopListsByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where phone equals to DEFAULT_PHONE
        defaultShopListShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the shopListList where phone equals to UPDATED_PHONE
        defaultShopListShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllShopListsByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultShopListShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the shopListList where phone equals to UPDATED_PHONE
        defaultShopListShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllShopListsByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where phone is not null
        defaultShopListShouldBeFound("phone.specified=true");

        // Get all the shopListList where phone is null
        defaultShopListShouldNotBeFound("phone.specified=false");
    }

    @Test
    @Transactional
    public void getAllShopListsByCreateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where createDate equals to DEFAULT_CREATE_DATE
        defaultShopListShouldBeFound("createDate.equals=" + DEFAULT_CREATE_DATE);

        // Get all the shopListList where createDate equals to UPDATED_CREATE_DATE
        defaultShopListShouldNotBeFound("createDate.equals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllShopListsByCreateDateIsInShouldWork() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where createDate in DEFAULT_CREATE_DATE or UPDATED_CREATE_DATE
        defaultShopListShouldBeFound("createDate.in=" + DEFAULT_CREATE_DATE + "," + UPDATED_CREATE_DATE);

        // Get all the shopListList where createDate equals to UPDATED_CREATE_DATE
        defaultShopListShouldNotBeFound("createDate.in=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllShopListsByCreateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where createDate is not null
        defaultShopListShouldBeFound("createDate.specified=true");

        // Get all the shopListList where createDate is null
        defaultShopListShouldNotBeFound("createDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllShopListsByCreateDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where createDate greater than or equals to DEFAULT_CREATE_DATE
        defaultShopListShouldBeFound("createDate.greaterOrEqualThan=" + DEFAULT_CREATE_DATE);

        // Get all the shopListList where createDate greater than or equals to UPDATED_CREATE_DATE
        defaultShopListShouldNotBeFound("createDate.greaterOrEqualThan=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllShopListsByCreateDateIsLessThanSomething() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);

        // Get all the shopListList where createDate less than or equals to DEFAULT_CREATE_DATE
        defaultShopListShouldNotBeFound("createDate.lessThan=" + DEFAULT_CREATE_DATE);

        // Get all the shopListList where createDate less than or equals to UPDATED_CREATE_DATE
        defaultShopListShouldBeFound("createDate.lessThan=" + UPDATED_CREATE_DATE);
    }


    @Test
    @Transactional
    public void getAllShopListsByShopItemsIsEqualToSomething() throws Exception {
        // Initialize the database
        ShopItem shopItems = ShopItemResourceIntTest.createEntity(em);
        em.persist(shopItems);
        em.flush();
        shopList.addShopItems(shopItems);
        shopListRepository.saveAndFlush(shopList);
        Long shopItemsId = shopItems.getId();

        // Get all the shopListList where shopItems equals to shopItemsId
        defaultShopListShouldBeFound("shopItemsId.equals=" + shopItemsId);

        // Get all the shopListList where shopItems equals to shopItemsId + 1
        defaultShopListShouldNotBeFound("shopItemsId.equals=" + (shopItemsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultShopListShouldBeFound(String filter) throws Exception {
        restShopListMockMvc.perform(get("/api/shop-lists?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopList.getId().intValue())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultShopListShouldNotBeFound(String filter) throws Exception {
        restShopListMockMvc.perform(get("/api/shop-lists?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingShopList() throws Exception {
        // Get the shopList
        restShopListMockMvc.perform(get("/api/shop-lists/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShopList() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);
        int databaseSizeBeforeUpdate = shopListRepository.findAll().size();

        // Update the shopList
        ShopList updatedShopList = shopListRepository.findOne(shopList.getId());
        // Disconnect from session so that the updates on updatedShopList are not directly saved in db
        em.detach(updatedShopList);
        updatedShopList
            .fullName(UPDATED_FULL_NAME)
            .address(UPDATED_ADDRESS)
            .phone(UPDATED_PHONE)
            .createDate(UPDATED_CREATE_DATE);
        ShopListDTO shopListDTO = shopListMapper.toDto(updatedShopList);

        restShopListMockMvc.perform(put("/api/shop-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopListDTO)))
            .andExpect(status().isOk());

        // Validate the ShopList in the database
        List<ShopList> shopListList = shopListRepository.findAll();
        assertThat(shopListList).hasSize(databaseSizeBeforeUpdate);
        ShopList testShopList = shopListList.get(shopListList.size() - 1);
        assertThat(testShopList.getFullName()).isEqualTo(UPDATED_FULL_NAME);
        assertThat(testShopList.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testShopList.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testShopList.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingShopList() throws Exception {
        int databaseSizeBeforeUpdate = shopListRepository.findAll().size();

        // Create the ShopList
        ShopListDTO shopListDTO = shopListMapper.toDto(shopList);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restShopListMockMvc.perform(put("/api/shop-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopListDTO)))
            .andExpect(status().isCreated());

        // Validate the ShopList in the database
        List<ShopList> shopListList = shopListRepository.findAll();
        assertThat(shopListList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteShopList() throws Exception {
        // Initialize the database
        shopListRepository.saveAndFlush(shopList);
        int databaseSizeBeforeDelete = shopListRepository.findAll().size();

        // Get the shopList
        restShopListMockMvc.perform(delete("/api/shop-lists/{id}", shopList.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ShopList> shopListList = shopListRepository.findAll();
        assertThat(shopListList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopList.class);
        ShopList shopList1 = new ShopList();
        shopList1.setId(1L);
        ShopList shopList2 = new ShopList();
        shopList2.setId(shopList1.getId());
        assertThat(shopList1).isEqualTo(shopList2);
        shopList2.setId(2L);
        assertThat(shopList1).isNotEqualTo(shopList2);
        shopList1.setId(null);
        assertThat(shopList1).isNotEqualTo(shopList2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopListDTO.class);
        ShopListDTO shopListDTO1 = new ShopListDTO();
        shopListDTO1.setId(1L);
        ShopListDTO shopListDTO2 = new ShopListDTO();
        assertThat(shopListDTO1).isNotEqualTo(shopListDTO2);
        shopListDTO2.setId(shopListDTO1.getId());
        assertThat(shopListDTO1).isEqualTo(shopListDTO2);
        shopListDTO2.setId(2L);
        assertThat(shopListDTO1).isNotEqualTo(shopListDTO2);
        shopListDTO1.setId(null);
        assertThat(shopListDTO1).isNotEqualTo(shopListDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(shopListMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(shopListMapper.fromId(null)).isNull();
    }
}
