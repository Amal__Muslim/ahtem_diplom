package com.diplom.app.web.rest;

import com.diplom.app.AhtemDiplomApp;

import com.diplom.app.domain.Product;
import com.diplom.app.domain.ShopItem;
import com.diplom.app.repository.ProductRepository;
import com.diplom.app.service.ProductService;
import com.diplom.app.service.dto.ProductDTO;
import com.diplom.app.service.mapper.ProductMapper;
import com.diplom.app.web.rest.errors.ExceptionTranslator;
import com.diplom.app.service.dto.ProductCriteria;
import com.diplom.app.service.ProductQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.diplom.app.web.rest.TestUtil.sameInstant;
import static com.diplom.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProductResource REST controller.
 *
 * @see ProductResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AhtemDiplomApp.class)
public class ProductResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_PRICE = 1;
    private static final Integer UPDATED_PRICE = 2;

    private static final Integer DEFAULT_PREVIOUS_PRICE = 1;
    private static final Integer UPDATED_PREVIOUS_PRICE = 2;

    private static final Integer DEFAULT_COUNT = 1;
    private static final Integer UPDATED_COUNT = 2;

    private static final Integer DEFAULT_STARS = 1;
    private static final Integer UPDATED_STARS = 2;

    private static final String DEFAULT_DETAILS = "AAAAAAAAAA";
    private static final String UPDATED_DETAILS = "BBBBBBBBBB";

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductQueryService productQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProductMockMvc;

    private Product product;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProductResource productResource = new ProductResource(productService, productQueryService);
        this.restProductMockMvc = MockMvcBuilders.standaloneSetup(productResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Product createEntity(EntityManager em) {
        Product product = new Product()
            .name(DEFAULT_NAME)
            .price(DEFAULT_PRICE)
            .previousPrice(DEFAULT_PREVIOUS_PRICE)
            .count(DEFAULT_COUNT)
            .stars(DEFAULT_STARS)
            .details(DEFAULT_DETAILS)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE)
            .createDate(DEFAULT_CREATE_DATE);
        return product;
    }

    @Before
    public void initTest() {
        product = createEntity(em);
    }

    @Test
    @Transactional
    public void createProduct() throws Exception {
        int databaseSizeBeforeCreate = productRepository.findAll().size();

        // Create the Product
        ProductDTO productDTO = productMapper.toDto(product);
        restProductMockMvc.perform(post("/api/products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isCreated());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeCreate + 1);
        Product testProduct = productList.get(productList.size() - 1);
        assertThat(testProduct.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProduct.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testProduct.getPreviousPrice()).isEqualTo(DEFAULT_PREVIOUS_PRICE);
        assertThat(testProduct.getCount()).isEqualTo(DEFAULT_COUNT);
        assertThat(testProduct.getStars()).isEqualTo(DEFAULT_STARS);
        assertThat(testProduct.getDetails()).isEqualTo(DEFAULT_DETAILS);
        assertThat(testProduct.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testProduct.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testProduct.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
    }

    @Test
    @Transactional
    public void createProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productRepository.findAll().size();

        // Create the Product with an existing ID
        product.setId(1L);
        ProductDTO productDTO = productMapper.toDto(product);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductMockMvc.perform(post("/api/products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllProducts() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList
        restProductMockMvc.perform(get("/api/products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(product.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.[*].previousPrice").value(hasItem(DEFAULT_PREVIOUS_PRICE)))
            .andExpect(jsonPath("$.[*].count").value(hasItem(DEFAULT_COUNT)))
            .andExpect(jsonPath("$.[*].stars").value(hasItem(DEFAULT_STARS)))
            .andExpect(jsonPath("$.[*].details").value(hasItem(DEFAULT_DETAILS.toString())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))));
    }

    @Test
    @Transactional
    public void getProduct() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get the product
        restProductMockMvc.perform(get("/api/products/{id}", product.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(product.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE))
            .andExpect(jsonPath("$.previousPrice").value(DEFAULT_PREVIOUS_PRICE))
            .andExpect(jsonPath("$.count").value(DEFAULT_COUNT))
            .andExpect(jsonPath("$.stars").value(DEFAULT_STARS))
            .andExpect(jsonPath("$.details").value(DEFAULT_DETAILS.toString()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)));
    }

    @Test
    @Transactional
    public void getAllProductsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where name equals to DEFAULT_NAME
        defaultProductShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the productList where name equals to UPDATED_NAME
        defaultProductShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProductsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where name in DEFAULT_NAME or UPDATED_NAME
        defaultProductShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the productList where name equals to UPDATED_NAME
        defaultProductShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProductsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where name is not null
        defaultProductShouldBeFound("name.specified=true");

        // Get all the productList where name is null
        defaultProductShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price equals to DEFAULT_PRICE
        defaultProductShouldBeFound("price.equals=" + DEFAULT_PRICE);

        // Get all the productList where price equals to UPDATED_PRICE
        defaultProductShouldNotBeFound("price.equals=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price in DEFAULT_PRICE or UPDATED_PRICE
        defaultProductShouldBeFound("price.in=" + DEFAULT_PRICE + "," + UPDATED_PRICE);

        // Get all the productList where price equals to UPDATED_PRICE
        defaultProductShouldNotBeFound("price.in=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price is not null
        defaultProductShouldBeFound("price.specified=true");

        // Get all the productList where price is null
        defaultProductShouldNotBeFound("price.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price greater than or equals to DEFAULT_PRICE
        defaultProductShouldBeFound("price.greaterOrEqualThan=" + DEFAULT_PRICE);

        // Get all the productList where price greater than or equals to UPDATED_PRICE
        defaultProductShouldNotBeFound("price.greaterOrEqualThan=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price less than or equals to DEFAULT_PRICE
        defaultProductShouldNotBeFound("price.lessThan=" + DEFAULT_PRICE);

        // Get all the productList where price less than or equals to UPDATED_PRICE
        defaultProductShouldBeFound("price.lessThan=" + UPDATED_PRICE);
    }


    @Test
    @Transactional
    public void getAllProductsByPreviousPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where previousPrice equals to DEFAULT_PREVIOUS_PRICE
        defaultProductShouldBeFound("previousPrice.equals=" + DEFAULT_PREVIOUS_PRICE);

        // Get all the productList where previousPrice equals to UPDATED_PREVIOUS_PRICE
        defaultProductShouldNotBeFound("previousPrice.equals=" + UPDATED_PREVIOUS_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPreviousPriceIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where previousPrice in DEFAULT_PREVIOUS_PRICE or UPDATED_PREVIOUS_PRICE
        defaultProductShouldBeFound("previousPrice.in=" + DEFAULT_PREVIOUS_PRICE + "," + UPDATED_PREVIOUS_PRICE);

        // Get all the productList where previousPrice equals to UPDATED_PREVIOUS_PRICE
        defaultProductShouldNotBeFound("previousPrice.in=" + UPDATED_PREVIOUS_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPreviousPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where previousPrice is not null
        defaultProductShouldBeFound("previousPrice.specified=true");

        // Get all the productList where previousPrice is null
        defaultProductShouldNotBeFound("previousPrice.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByPreviousPriceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where previousPrice greater than or equals to DEFAULT_PREVIOUS_PRICE
        defaultProductShouldBeFound("previousPrice.greaterOrEqualThan=" + DEFAULT_PREVIOUS_PRICE);

        // Get all the productList where previousPrice greater than or equals to UPDATED_PREVIOUS_PRICE
        defaultProductShouldNotBeFound("previousPrice.greaterOrEqualThan=" + UPDATED_PREVIOUS_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPreviousPriceIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where previousPrice less than or equals to DEFAULT_PREVIOUS_PRICE
        defaultProductShouldNotBeFound("previousPrice.lessThan=" + DEFAULT_PREVIOUS_PRICE);

        // Get all the productList where previousPrice less than or equals to UPDATED_PREVIOUS_PRICE
        defaultProductShouldBeFound("previousPrice.lessThan=" + UPDATED_PREVIOUS_PRICE);
    }


    @Test
    @Transactional
    public void getAllProductsByCountIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where count equals to DEFAULT_COUNT
        defaultProductShouldBeFound("count.equals=" + DEFAULT_COUNT);

        // Get all the productList where count equals to UPDATED_COUNT
        defaultProductShouldNotBeFound("count.equals=" + UPDATED_COUNT);
    }

    @Test
    @Transactional
    public void getAllProductsByCountIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where count in DEFAULT_COUNT or UPDATED_COUNT
        defaultProductShouldBeFound("count.in=" + DEFAULT_COUNT + "," + UPDATED_COUNT);

        // Get all the productList where count equals to UPDATED_COUNT
        defaultProductShouldNotBeFound("count.in=" + UPDATED_COUNT);
    }

    @Test
    @Transactional
    public void getAllProductsByCountIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where count is not null
        defaultProductShouldBeFound("count.specified=true");

        // Get all the productList where count is null
        defaultProductShouldNotBeFound("count.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByCountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where count greater than or equals to DEFAULT_COUNT
        defaultProductShouldBeFound("count.greaterOrEqualThan=" + DEFAULT_COUNT);

        // Get all the productList where count greater than or equals to UPDATED_COUNT
        defaultProductShouldNotBeFound("count.greaterOrEqualThan=" + UPDATED_COUNT);
    }

    @Test
    @Transactional
    public void getAllProductsByCountIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where count less than or equals to DEFAULT_COUNT
        defaultProductShouldNotBeFound("count.lessThan=" + DEFAULT_COUNT);

        // Get all the productList where count less than or equals to UPDATED_COUNT
        defaultProductShouldBeFound("count.lessThan=" + UPDATED_COUNT);
    }


    @Test
    @Transactional
    public void getAllProductsByStarsIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stars equals to DEFAULT_STARS
        defaultProductShouldBeFound("stars.equals=" + DEFAULT_STARS);

        // Get all the productList where stars equals to UPDATED_STARS
        defaultProductShouldNotBeFound("stars.equals=" + UPDATED_STARS);
    }

    @Test
    @Transactional
    public void getAllProductsByStarsIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stars in DEFAULT_STARS or UPDATED_STARS
        defaultProductShouldBeFound("stars.in=" + DEFAULT_STARS + "," + UPDATED_STARS);

        // Get all the productList where stars equals to UPDATED_STARS
        defaultProductShouldNotBeFound("stars.in=" + UPDATED_STARS);
    }

    @Test
    @Transactional
    public void getAllProductsByStarsIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stars is not null
        defaultProductShouldBeFound("stars.specified=true");

        // Get all the productList where stars is null
        defaultProductShouldNotBeFound("stars.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByStarsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stars greater than or equals to DEFAULT_STARS
        defaultProductShouldBeFound("stars.greaterOrEqualThan=" + DEFAULT_STARS);

        // Get all the productList where stars greater than or equals to UPDATED_STARS
        defaultProductShouldNotBeFound("stars.greaterOrEqualThan=" + UPDATED_STARS);
    }

    @Test
    @Transactional
    public void getAllProductsByStarsIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stars less than or equals to DEFAULT_STARS
        defaultProductShouldNotBeFound("stars.lessThan=" + DEFAULT_STARS);

        // Get all the productList where stars less than or equals to UPDATED_STARS
        defaultProductShouldBeFound("stars.lessThan=" + UPDATED_STARS);
    }


    @Test
    @Transactional
    public void getAllProductsByDetailsIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where details equals to DEFAULT_DETAILS
        defaultProductShouldBeFound("details.equals=" + DEFAULT_DETAILS);

        // Get all the productList where details equals to UPDATED_DETAILS
        defaultProductShouldNotBeFound("details.equals=" + UPDATED_DETAILS);
    }

    @Test
    @Transactional
    public void getAllProductsByDetailsIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where details in DEFAULT_DETAILS or UPDATED_DETAILS
        defaultProductShouldBeFound("details.in=" + DEFAULT_DETAILS + "," + UPDATED_DETAILS);

        // Get all the productList where details equals to UPDATED_DETAILS
        defaultProductShouldNotBeFound("details.in=" + UPDATED_DETAILS);
    }

    @Test
    @Transactional
    public void getAllProductsByDetailsIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where details is not null
        defaultProductShouldBeFound("details.specified=true");

        // Get all the productList where details is null
        defaultProductShouldNotBeFound("details.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByCreateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where createDate equals to DEFAULT_CREATE_DATE
        defaultProductShouldBeFound("createDate.equals=" + DEFAULT_CREATE_DATE);

        // Get all the productList where createDate equals to UPDATED_CREATE_DATE
        defaultProductShouldNotBeFound("createDate.equals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllProductsByCreateDateIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where createDate in DEFAULT_CREATE_DATE or UPDATED_CREATE_DATE
        defaultProductShouldBeFound("createDate.in=" + DEFAULT_CREATE_DATE + "," + UPDATED_CREATE_DATE);

        // Get all the productList where createDate equals to UPDATED_CREATE_DATE
        defaultProductShouldNotBeFound("createDate.in=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllProductsByCreateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where createDate is not null
        defaultProductShouldBeFound("createDate.specified=true");

        // Get all the productList where createDate is null
        defaultProductShouldNotBeFound("createDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByCreateDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where createDate greater than or equals to DEFAULT_CREATE_DATE
        defaultProductShouldBeFound("createDate.greaterOrEqualThan=" + DEFAULT_CREATE_DATE);

        // Get all the productList where createDate greater than or equals to UPDATED_CREATE_DATE
        defaultProductShouldNotBeFound("createDate.greaterOrEqualThan=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllProductsByCreateDateIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where createDate less than or equals to DEFAULT_CREATE_DATE
        defaultProductShouldNotBeFound("createDate.lessThan=" + DEFAULT_CREATE_DATE);

        // Get all the productList where createDate less than or equals to UPDATED_CREATE_DATE
        defaultProductShouldBeFound("createDate.lessThan=" + UPDATED_CREATE_DATE);
    }


    @Test
    @Transactional
    public void getAllProductsByShopItemsIsEqualToSomething() throws Exception {
        // Initialize the database
        ShopItem shopItems = ShopItemResourceIntTest.createEntity(em);
        em.persist(shopItems);
        em.flush();
        product.addShopItems(shopItems);
        productRepository.saveAndFlush(product);
        Long shopItemsId = shopItems.getId();

        // Get all the productList where shopItems equals to shopItemsId
        defaultProductShouldBeFound("shopItemsId.equals=" + shopItemsId);

        // Get all the productList where shopItems equals to shopItemsId + 1
        defaultProductShouldNotBeFound("shopItemsId.equals=" + (shopItemsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultProductShouldBeFound(String filter) throws Exception {
        restProductMockMvc.perform(get("/api/products?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(product.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.[*].previousPrice").value(hasItem(DEFAULT_PREVIOUS_PRICE)))
            .andExpect(jsonPath("$.[*].count").value(hasItem(DEFAULT_COUNT)))
            .andExpect(jsonPath("$.[*].stars").value(hasItem(DEFAULT_STARS)))
            .andExpect(jsonPath("$.[*].details").value(hasItem(DEFAULT_DETAILS.toString())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultProductShouldNotBeFound(String filter) throws Exception {
        restProductMockMvc.perform(get("/api/products?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingProduct() throws Exception {
        // Get the product
        restProductMockMvc.perform(get("/api/products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProduct() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        int databaseSizeBeforeUpdate = productRepository.findAll().size();

        // Update the product
        Product updatedProduct = productRepository.findOne(product.getId());
        // Disconnect from session so that the updates on updatedProduct are not directly saved in db
        em.detach(updatedProduct);
        updatedProduct
            .name(UPDATED_NAME)
            .price(UPDATED_PRICE)
            .previousPrice(UPDATED_PREVIOUS_PRICE)
            .count(UPDATED_COUNT)
            .stars(UPDATED_STARS)
            .details(UPDATED_DETAILS)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .createDate(UPDATED_CREATE_DATE);
        ProductDTO productDTO = productMapper.toDto(updatedProduct);

        restProductMockMvc.perform(put("/api/products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isOk());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeUpdate);
        Product testProduct = productList.get(productList.size() - 1);
        assertThat(testProduct.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProduct.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testProduct.getPreviousPrice()).isEqualTo(UPDATED_PREVIOUS_PRICE);
        assertThat(testProduct.getCount()).isEqualTo(UPDATED_COUNT);
        assertThat(testProduct.getStars()).isEqualTo(UPDATED_STARS);
        assertThat(testProduct.getDetails()).isEqualTo(UPDATED_DETAILS);
        assertThat(testProduct.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testProduct.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testProduct.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProduct() throws Exception {
        int databaseSizeBeforeUpdate = productRepository.findAll().size();

        // Create the Product
        ProductDTO productDTO = productMapper.toDto(product);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProductMockMvc.perform(put("/api/products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isCreated());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteProduct() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        int databaseSizeBeforeDelete = productRepository.findAll().size();

        // Get the product
        restProductMockMvc.perform(delete("/api/products/{id}", product.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Product.class);
        Product product1 = new Product();
        product1.setId(1L);
        Product product2 = new Product();
        product2.setId(product1.getId());
        assertThat(product1).isEqualTo(product2);
        product2.setId(2L);
        assertThat(product1).isNotEqualTo(product2);
        product1.setId(null);
        assertThat(product1).isNotEqualTo(product2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductDTO.class);
        ProductDTO productDTO1 = new ProductDTO();
        productDTO1.setId(1L);
        ProductDTO productDTO2 = new ProductDTO();
        assertThat(productDTO1).isNotEqualTo(productDTO2);
        productDTO2.setId(productDTO1.getId());
        assertThat(productDTO1).isEqualTo(productDTO2);
        productDTO2.setId(2L);
        assertThat(productDTO1).isNotEqualTo(productDTO2);
        productDTO1.setId(null);
        assertThat(productDTO1).isNotEqualTo(productDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(productMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(productMapper.fromId(null)).isNull();
    }
}
