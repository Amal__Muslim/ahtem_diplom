package com.diplom.app.web.rest;

import com.diplom.app.AhtemDiplomApp;

import com.diplom.app.domain.ShopItem;
import com.diplom.app.domain.ShopList;
import com.diplom.app.domain.Product;
import com.diplom.app.repository.ShopItemRepository;
import com.diplom.app.service.ShopItemService;
import com.diplom.app.service.dto.ShopItemDTO;
import com.diplom.app.service.mapper.ShopItemMapper;
import com.diplom.app.web.rest.errors.ExceptionTranslator;
import com.diplom.app.service.dto.ShopItemCriteria;
import com.diplom.app.service.ShopItemQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.diplom.app.web.rest.TestUtil.sameInstant;
import static com.diplom.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ShopItemResource REST controller.
 *
 * @see ShopItemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AhtemDiplomApp.class)
public class ShopItemResourceIntTest {

    private static final Integer DEFAULT_COUNT = 1;
    private static final Integer UPDATED_COUNT = 2;

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private ShopItemRepository shopItemRepository;

    @Autowired
    private ShopItemMapper shopItemMapper;

    @Autowired
    private ShopItemService shopItemService;

    @Autowired
    private ShopItemQueryService shopItemQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restShopItemMockMvc;

    private ShopItem shopItem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShopItemResource shopItemResource = new ShopItemResource(shopItemService, shopItemQueryService);
        this.restShopItemMockMvc = MockMvcBuilders.standaloneSetup(shopItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopItem createEntity(EntityManager em) {
        ShopItem shopItem = new ShopItem()
            .count(DEFAULT_COUNT)
            .createDate(DEFAULT_CREATE_DATE);
        return shopItem;
    }

    @Before
    public void initTest() {
        shopItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createShopItem() throws Exception {
        int databaseSizeBeforeCreate = shopItemRepository.findAll().size();

        // Create the ShopItem
        ShopItemDTO shopItemDTO = shopItemMapper.toDto(shopItem);
        restShopItemMockMvc.perform(post("/api/shop-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopItemDTO)))
            .andExpect(status().isCreated());

        // Validate the ShopItem in the database
        List<ShopItem> shopItemList = shopItemRepository.findAll();
        assertThat(shopItemList).hasSize(databaseSizeBeforeCreate + 1);
        ShopItem testShopItem = shopItemList.get(shopItemList.size() - 1);
        assertThat(testShopItem.getCount()).isEqualTo(DEFAULT_COUNT);
        assertThat(testShopItem.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
    }

    @Test
    @Transactional
    public void createShopItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shopItemRepository.findAll().size();

        // Create the ShopItem with an existing ID
        shopItem.setId(1L);
        ShopItemDTO shopItemDTO = shopItemMapper.toDto(shopItem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShopItemMockMvc.perform(post("/api/shop-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShopItem in the database
        List<ShopItem> shopItemList = shopItemRepository.findAll();
        assertThat(shopItemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllShopItems() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList
        restShopItemMockMvc.perform(get("/api/shop-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].count").value(hasItem(DEFAULT_COUNT)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))));
    }

    @Test
    @Transactional
    public void getShopItem() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get the shopItem
        restShopItemMockMvc.perform(get("/api/shop-items/{id}", shopItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shopItem.getId().intValue()))
            .andExpect(jsonPath("$.count").value(DEFAULT_COUNT))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)));
    }

    @Test
    @Transactional
    public void getAllShopItemsByCountIsEqualToSomething() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList where count equals to DEFAULT_COUNT
        defaultShopItemShouldBeFound("count.equals=" + DEFAULT_COUNT);

        // Get all the shopItemList where count equals to UPDATED_COUNT
        defaultShopItemShouldNotBeFound("count.equals=" + UPDATED_COUNT);
    }

    @Test
    @Transactional
    public void getAllShopItemsByCountIsInShouldWork() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList where count in DEFAULT_COUNT or UPDATED_COUNT
        defaultShopItemShouldBeFound("count.in=" + DEFAULT_COUNT + "," + UPDATED_COUNT);

        // Get all the shopItemList where count equals to UPDATED_COUNT
        defaultShopItemShouldNotBeFound("count.in=" + UPDATED_COUNT);
    }

    @Test
    @Transactional
    public void getAllShopItemsByCountIsNullOrNotNull() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList where count is not null
        defaultShopItemShouldBeFound("count.specified=true");

        // Get all the shopItemList where count is null
        defaultShopItemShouldNotBeFound("count.specified=false");
    }

    @Test
    @Transactional
    public void getAllShopItemsByCountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList where count greater than or equals to DEFAULT_COUNT
        defaultShopItemShouldBeFound("count.greaterOrEqualThan=" + DEFAULT_COUNT);

        // Get all the shopItemList where count greater than or equals to UPDATED_COUNT
        defaultShopItemShouldNotBeFound("count.greaterOrEqualThan=" + UPDATED_COUNT);
    }

    @Test
    @Transactional
    public void getAllShopItemsByCountIsLessThanSomething() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList where count less than or equals to DEFAULT_COUNT
        defaultShopItemShouldNotBeFound("count.lessThan=" + DEFAULT_COUNT);

        // Get all the shopItemList where count less than or equals to UPDATED_COUNT
        defaultShopItemShouldBeFound("count.lessThan=" + UPDATED_COUNT);
    }


    @Test
    @Transactional
    public void getAllShopItemsByCreateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList where createDate equals to DEFAULT_CREATE_DATE
        defaultShopItemShouldBeFound("createDate.equals=" + DEFAULT_CREATE_DATE);

        // Get all the shopItemList where createDate equals to UPDATED_CREATE_DATE
        defaultShopItemShouldNotBeFound("createDate.equals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllShopItemsByCreateDateIsInShouldWork() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList where createDate in DEFAULT_CREATE_DATE or UPDATED_CREATE_DATE
        defaultShopItemShouldBeFound("createDate.in=" + DEFAULT_CREATE_DATE + "," + UPDATED_CREATE_DATE);

        // Get all the shopItemList where createDate equals to UPDATED_CREATE_DATE
        defaultShopItemShouldNotBeFound("createDate.in=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllShopItemsByCreateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList where createDate is not null
        defaultShopItemShouldBeFound("createDate.specified=true");

        // Get all the shopItemList where createDate is null
        defaultShopItemShouldNotBeFound("createDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllShopItemsByCreateDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList where createDate greater than or equals to DEFAULT_CREATE_DATE
        defaultShopItemShouldBeFound("createDate.greaterOrEqualThan=" + DEFAULT_CREATE_DATE);

        // Get all the shopItemList where createDate greater than or equals to UPDATED_CREATE_DATE
        defaultShopItemShouldNotBeFound("createDate.greaterOrEqualThan=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllShopItemsByCreateDateIsLessThanSomething() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);

        // Get all the shopItemList where createDate less than or equals to DEFAULT_CREATE_DATE
        defaultShopItemShouldNotBeFound("createDate.lessThan=" + DEFAULT_CREATE_DATE);

        // Get all the shopItemList where createDate less than or equals to UPDATED_CREATE_DATE
        defaultShopItemShouldBeFound("createDate.lessThan=" + UPDATED_CREATE_DATE);
    }


    @Test
    @Transactional
    public void getAllShopItemsByShopListIsEqualToSomething() throws Exception {
        // Initialize the database
        ShopList shopList = ShopListResourceIntTest.createEntity(em);
        em.persist(shopList);
        em.flush();
        shopItem.setShopList(shopList);
        shopItemRepository.saveAndFlush(shopItem);
        Long shopListId = shopList.getId();

        // Get all the shopItemList where shopList equals to shopListId
        defaultShopItemShouldBeFound("shopListId.equals=" + shopListId);

        // Get all the shopItemList where shopList equals to shopListId + 1
        defaultShopItemShouldNotBeFound("shopListId.equals=" + (shopListId + 1));
    }


    @Test
    @Transactional
    public void getAllShopItemsByProductIsEqualToSomething() throws Exception {
        // Initialize the database
        Product product = ProductResourceIntTest.createEntity(em);
        em.persist(product);
        em.flush();
        shopItem.setProduct(product);
        shopItemRepository.saveAndFlush(shopItem);
        Long productId = product.getId();

        // Get all the shopItemList where product equals to productId
        defaultShopItemShouldBeFound("productId.equals=" + productId);

        // Get all the shopItemList where product equals to productId + 1
        defaultShopItemShouldNotBeFound("productId.equals=" + (productId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultShopItemShouldBeFound(String filter) throws Exception {
        restShopItemMockMvc.perform(get("/api/shop-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].count").value(hasItem(DEFAULT_COUNT)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultShopItemShouldNotBeFound(String filter) throws Exception {
        restShopItemMockMvc.perform(get("/api/shop-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingShopItem() throws Exception {
        // Get the shopItem
        restShopItemMockMvc.perform(get("/api/shop-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShopItem() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);
        int databaseSizeBeforeUpdate = shopItemRepository.findAll().size();

        // Update the shopItem
        ShopItem updatedShopItem = shopItemRepository.findOne(shopItem.getId());
        // Disconnect from session so that the updates on updatedShopItem are not directly saved in db
        em.detach(updatedShopItem);
        updatedShopItem
            .count(UPDATED_COUNT)
            .createDate(UPDATED_CREATE_DATE);
        ShopItemDTO shopItemDTO = shopItemMapper.toDto(updatedShopItem);

        restShopItemMockMvc.perform(put("/api/shop-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopItemDTO)))
            .andExpect(status().isOk());

        // Validate the ShopItem in the database
        List<ShopItem> shopItemList = shopItemRepository.findAll();
        assertThat(shopItemList).hasSize(databaseSizeBeforeUpdate);
        ShopItem testShopItem = shopItemList.get(shopItemList.size() - 1);
        assertThat(testShopItem.getCount()).isEqualTo(UPDATED_COUNT);
        assertThat(testShopItem.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingShopItem() throws Exception {
        int databaseSizeBeforeUpdate = shopItemRepository.findAll().size();

        // Create the ShopItem
        ShopItemDTO shopItemDTO = shopItemMapper.toDto(shopItem);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restShopItemMockMvc.perform(put("/api/shop-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopItemDTO)))
            .andExpect(status().isCreated());

        // Validate the ShopItem in the database
        List<ShopItem> shopItemList = shopItemRepository.findAll();
        assertThat(shopItemList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteShopItem() throws Exception {
        // Initialize the database
        shopItemRepository.saveAndFlush(shopItem);
        int databaseSizeBeforeDelete = shopItemRepository.findAll().size();

        // Get the shopItem
        restShopItemMockMvc.perform(delete("/api/shop-items/{id}", shopItem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ShopItem> shopItemList = shopItemRepository.findAll();
        assertThat(shopItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopItem.class);
        ShopItem shopItem1 = new ShopItem();
        shopItem1.setId(1L);
        ShopItem shopItem2 = new ShopItem();
        shopItem2.setId(shopItem1.getId());
        assertThat(shopItem1).isEqualTo(shopItem2);
        shopItem2.setId(2L);
        assertThat(shopItem1).isNotEqualTo(shopItem2);
        shopItem1.setId(null);
        assertThat(shopItem1).isNotEqualTo(shopItem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopItemDTO.class);
        ShopItemDTO shopItemDTO1 = new ShopItemDTO();
        shopItemDTO1.setId(1L);
        ShopItemDTO shopItemDTO2 = new ShopItemDTO();
        assertThat(shopItemDTO1).isNotEqualTo(shopItemDTO2);
        shopItemDTO2.setId(shopItemDTO1.getId());
        assertThat(shopItemDTO1).isEqualTo(shopItemDTO2);
        shopItemDTO2.setId(2L);
        assertThat(shopItemDTO1).isNotEqualTo(shopItemDTO2);
        shopItemDTO1.setId(null);
        assertThat(shopItemDTO1).isNotEqualTo(shopItemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(shopItemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(shopItemMapper.fromId(null)).isNull();
    }
}
